FROM node:8.11.3

COPY . /bundle
RUN (cd /bundle/programs/server && npm i)

USER node

ENV NODE_ENV=production

CMD node /bundle/main.js